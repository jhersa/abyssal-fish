# Abyssal Fish

## Golang
- port: 1337
- go run app.go

## NodeJS
- port: 3000
- node app.js

## Python
- port: 5000
- pip install flask
- python app.py

## Nginx
/etc/nginx/conf.d/default.conf
Balanceo de las tres aplicaciones a traves de `proxy_pass`, `load balancer`

80 -> 1337,3000,5000